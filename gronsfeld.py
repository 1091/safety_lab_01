##########################################
#Safety lab01
#Gronsfeld's cipher encryptor decryptor
#This is main file of this lab
#Fateev Ilya 1091 3 course
#Date: 18 February 2014
##########################################

#filling alphabet (list) that we use in open text
#alphabet list has the form: a[i] = 'some character'
#parameters: no parameters
#returns: list which represents alphabet
def fill_alphabet():
	alphabet = []
	ascii_A = ord('A')
	ascii_Z = ord('Z')
	ascii_a = ord('a')
	ascii_z = ord('z')
	ascii_space = ord(' ')
	ascii_atsign = ord('@')
	for i in range(ascii_A, ascii_Z+1):
		alphabet.append(chr(i))
	for i in range(ascii_a, ascii_z+1):
		alphabet.append(chr(i))
	for i in range(ascii_space, ascii_atsign + 1):
		alphabet.append(chr(i))
	alphabet.append('\n')
	return alphabet

#gronsfeld encryption function
#input: string to encrypt, string which stores cipher key, alphabet filled by
#function above
#returns: encrypted string
def gronsfeld_encryption(string, key, alphabet):
	key = list(key)
	key_current = 0
	encrypted = ""
	for i in range(len(string)):
		result_index = alphabet.index(string[i]) + int(key[key_current])
		if result_index > len(alphabet):
			encrypted += alphabet[result_index - len(alphabet)]
		else:
			encrypted += alphabet[result_index]
		if key_current == len(key) - 1:
			key_current = 0
		else:
			key_current += 1
	return encrypted

#gronsfeld decryption function
#input: string to encrypt, string which stores cipher key, alphabet filled by
#function above
#returns: decrypted string
def gronsfeld_decryption(string, key, alphabet):
	key = list(key)
	key = map(lambda key: int(key)*(-1), key)
	key_current = 0
	decrypted = ""
	for i in range(len(string)):
		result_index = alphabet.index(string[i]) + int(key[key_current])
		if result_index < 0:
			decrypted += alphabet[result_index + len(alphabet)]
		else:
			decrypted += alphabet[result_index]
		if key_current == len(key) - 1:
			key_current = 0
		else:
			key_current += 1
	return decrypted

#encrypts file
#input: file name, string with cipher key and filled alphabet
#returns: no return
def file_encrypting(input_file, cipher_key, alphabet):
	print "Encrypting..."
	file_to_encrypt = open(input_file, 'r')
	string_to_encrypt = file_to_encrypt.read() 
	file_to_encrypt.close() 
	encrypted = gronsfeld_encryption(string_to_encrypt, cipher_key, alphabet)
	file_to_encrypt = open(input_file, 'w')
	file_to_encrypt.write(encrypted)
	file_to_encrypt.close()

#decrypts file
#input: file name, string with cipher key and filled alphabet
#returns: no return
def file_decrypting(input_file, cipher_key, alphabet):
	print "Decrypting..."
	file_to_decrypt = open(input_file, 'r')
	string_to_decrypt = file_to_decrypt.read() 
	file_to_decrypt.close() 
	decrypted = gronsfeld_decryption(string_to_decrypt, cipher_key, alphabet)
	file_to_decrypt = open(input_file, 'w')
	file_to_decrypt.write(decrypted)

#parsing CLI args using argparse library
import argparse
parser = argparse.ArgumentParser(description="Gronsfeld cipher encryptor/decryptor")
parser.add_argument("input_file", help="file to encrypt/decrypt")
parser.add_argument("cipher_key", help="cipher key")
parser.add_argument("-d", help="if this option added, programm will be decrypting the file (default is encrypting)", action="store_true")

#obtaining arguments from parser
args = parser.parse_args()

#filling the alphabet
alphabet = fill_alphabet()

#encrypting/decrypting file, depends on '-d' argument (if '-d' added - decrypting, else encrypting)
if args.d:
	file_decrypting(args.input_file, args.cipher_key, alphabet)
else:
	file_encrypting(args.input_file, args.cipher_key, alphabet)

